package com.m3.training.interfacedemo;

public interface ICalculate {
	public Double evaluate(String input);
}
