package com.m3.training.interfacedemo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import com.m3.training.interfacedemo.Calculator;

class CalculatorTest {

	ICalculate objectUnderTest;
	
	@BeforeEach
	void setup() {
		objectUnderTest = new Calculator();
	}

	
	// not really a test
	@Test
	void test() {
		String msg = "Could not generate object of class under test.";
		assertNotNull(msg, objectUnderTest);
	}
	
	@Test
	void test_evaluate_alphabeticCharacters() {
		String msg = "Did not reject alphabetic characters.";
		String input = "Hello I am certainly a String.";
		Executable closure = () -> objectUnderTest.evaluate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
	
	@Test
	void test_evaluate_nullString() {
		String msg = "Did not reject null input.";
		String input = null;
		Executable closure = () -> objectUnderTest.evaluate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}

	@Test
	void test_evaluate_emptyString()  {
		String msg = "Did not reject empty String input.";
		String input = "";
		Executable closure = () -> objectUnderTest.evaluate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
	
	@Test
	void test_evaluate_whiteSpace()  {
		String msg = "Did not reject blank String input.";
		String input = "				   ";
		Executable closure = () -> objectUnderTest.evaluate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}

	@Test
	void test_evaluate_bufferOverflow()  {
		StringBuilder sb = new StringBuilder();
		for (int index = 0; index <= Calculator.CHAR_LIMIT + 5; index++) {
			sb.append("0");
		}
		String msg = "Did not reject overflow input.";
		String input = sb.toString();
		Executable closure = () -> objectUnderTest.evaluate(input);
		assertThrows(IllegalArgumentException.class, closure, msg);
	}
	
	@Test
	void test_evaluate_simplePositiveInput()  {
		String msg = "Did not respond with Double version of String containing one number.";
		String input = "1";
		Double actual;
		Double expected = 1.0;
		Double tolerance  = 0.00000002;
		actual = objectUnderTest.evaluate(input);
		assertEquals(msg, expected, actual, tolerance);
	}	
	
	@Test
	void test_evaluate_simpleNegativeInput()  {
		String msg = "Did not respond with Double version of String containing one negative number.";
		String input = "-1";
		Double actual;
		Double expected = -1.0;
		Double tolerance  = 0.00000002;
		actual = objectUnderTest.evaluate(input);
		assertEquals(msg, expected, actual, tolerance);
	}	
	
	
	
}
